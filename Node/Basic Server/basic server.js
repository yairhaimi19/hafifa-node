const http   = require('http');
const url    = require('url');
const config = require('./config/config.js');

const SERVER_PORT = process.env.SERVER_PORT || config.development.SERVER_PORT;
const POST_REQUEST = 'POST';

const isPrime = (num) => {
    if (num == 0) return false;

    for (let i = 2; i <= Math.sqrt(num); i++) {
        if (num % i == 0) {
            return false;
        }
    }

    return true;
}

const getPrimes = (numOfPrimes) => {
    let i       = 0;
    let currNum = 2;
    let primes  = '';
    
    while (i < numOfPrimes) {
        if (isPrime(currNum)) {
            primes += `${currNum} `;
            i++;
        }
        
        currNum++;
    }

    return primes;
}

const server = http.createServer((req, res) => {
    const urlParts = url.parse(req.url, true);

    if (req.url == '/') {
        res.write('Hello gamers');
        res.end();
    }

    if ((req.method == POST_REQUEST) && (req.url == '/api/numbers/prime/validate')) {
        let allPrime = true;
        let body = '';

        req.on('data', chunk => {
            body += chunk.toString();
        });
        req.on('end', () => {
            body = JSON.parse(body);

            for (const field in body) {
                if (body.hasOwnProperty(field)) {
                    if (!isPrime(body[field])) {
                        allPrime = false;
                    }
                }
            }
            res.write(allPrime.toString());
            res.end();
        });        
    }

    if (urlParts.pathname == '/api/numbers/prime') {
        const amount = urlParts.query.amount;

        if (amount <= 0) {
            res.end();
        } else if (amount > 32) {
            res.write(getPrimes(32));
            res.end();
        } else {
            res.write(getPrimes(amount));
            res.end();
        }
    }
});

server.listen(SERVER_PORT);
console.log(`listening on port ${SERVER_PORT}`);
const fs = require('fs');
const path = require('path');

const folderName = 'files_to_remove';
const logFileName = 'removed_files.txt';

const emptyDirectory = (dirPath, callback) => {
    let filesDeleted = '';

    if (!fs.existsSync(dirPath)) return;

    fs.readdir(dirPath, (err, files) => {
        if (err) throw err;

        files.forEach((file) => {
            filesDeleted = filesDeleted.concat(file).concat('\n');
            fs.unlink(path.join(dirPath, file), () => {
                if (err) throw err;
            });
        });

        callback(filesDeleted);
    });
};

setInterval(() => {
    fs.readdir(`./${folderName}`, (err, files) => {
        if (err) console.log(`error in reading dir: ${err}`);

        if (files.length !== 0) {
            emptyDirectory(`./${folderName}`, (filesDeleted) => {
                fs.appendFile(logFileName, filesDeleted, () => {
                    if (err) console.log(`error in appending file: ${err}`);
                    console.log(filesDeleted);
                });
            });
        }
    });
}, 500);

const express = require('express');
const config  = require('./config/config.js');

const app = express();

const port = process.env.SERVER_PORT || config.development.SERVER_PORT;

const isPrime = (num) => {
    if (num == 0) return false;

    for (let i = 2; i <= Math.sqrt(num); i++) {
        if (num % i == 0) {
            return false;
        }
    }

    return true;
}

const getPrimes = (numOfPrimes) => {
    let i       = 0;
    let currNum = 2;
    let primes  = '';
    
    while (i < numOfPrimes) {
        if (isPrime(currNum)) {
            primes += `${currNum} `;
            i++;
        }
        
        currNum++;
    }

    return primes;
}

app.use(express.json());
app.set('view engine', 'pug')

app.get('/', function (req, res) {
    res.render('index', { title: 'Hey', message: 'Hello there!' })
})

app.post('/api/numbers/prime/validate', (req, res) => {
    const body = req.body;
    let allPrime = true;

    for (const field in body) {
        if (body.hasOwnProperty(field)) {
            if (!isPrime(body[field])) {
                allPrime = false;
            }
        }
    }

    res.send(allPrime.toString());
});

app.get('/api/numbers/prime', (req, res) => {
    const amount = req.query.amount;
    console.log('h')

    if (amount <= 0) { 

    } else if (amount > 32) {
        res.write(getPrimes(32));
    } else {
        res.write(getPrimes(amount));
    }

    res.end();
});

app.get('/api/numbers/prime/display', (req, res) => {
    res.render('prime', {title: 'Prime Numbers', 
                         prime1: 2,  prime2:  3,
                         prime3: 5,  prime4:  7,
                         prime5: 11, prime6:  13,
                         prime7: 17, prime8:  19,
                         prime9: 23, prime10: 29});
})

app.listen(port, () => console.log(`Listening on port ${port}...`));
let args = (process.argv.slice(2));
let fs = require('fs');

const asciiSpace = 32;
const numOfFiles = args[0];
const numOfWords = args[1];


// 32 - 126
const randomAsciiChar = () => {
    const minPrintableAscii = asciiSpace;
    const maxPrintableAscii = 126;
    return Math.floor(Math.random() * (maxPrintableAscii + 1 - minPrintableAscii)) + minPrintableAscii;
}

const generateRandomWord = () => {
    let currentChar;
    let word = "";

    while (currentChar !== ' ') {
        currentChar = String.fromCharCode(randomAsciiChar());
        word += currentChar;
    }

    return word;
}

const generateWords = (n) => {
    let string = '';

    for (let i = 0; i < n; i++) {
        string += generateRandomWord();
    }

    return string;
}

fs.promises.mkdir('./created_files', { recursive: true });

for (let fileIndex = 0; fileIndex < numOfFiles; fileIndex++) {
    const wordMultiplier = fileIndex + 1;
    const fileContent = generateWords(numOfWords * wordMultiplier);

    fs.writeFile(`./created_files/file-${fileIndex + 1}.txt`, fileContent, (err) => {
        if (err) throw err;
        console.log(`file-${fileIndex + 1}: ${numOfWords * wordMultiplier}`);
    });
}